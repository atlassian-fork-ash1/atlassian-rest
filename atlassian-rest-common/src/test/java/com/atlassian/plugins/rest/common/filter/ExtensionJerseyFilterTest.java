package com.atlassian.plugins.rest.common.filter;

import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.spi.container.ContainerRequest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ExtensionJerseyFilterTest {
    private static final String ACCEPT_HEADER = "Accept";

    @Mock
    private ContainerRequest containerRequest;

    private ExtensionJerseyFilter jerseyFilter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        jerseyFilter = new ExtensionJerseyFilter(Collections.singletonList("/excluded.*"));
    }

    @Test
    public void testDefaultConstructor() throws Exception {
        jerseyFilter = new ExtensionJerseyFilter();
        final String baseUri = "http://localhost:8080/rest";
        final MultivaluedMap<String, String> parameters = new MultivaluedMapImpl();
        parameters.add("param1", "value1");
        parameters.add("param2", "value2.1");
        parameters.add("param2", "value2.2");
        testFilter(baseUri, parameters);
    }

    @Test
    public void testFilterWithoutQueryParameters() throws Exception {
        final String baseUri = "http://localhost:8080/rest";
        testFilter(baseUri, new MultivaluedMapImpl());
    }

    @Test
    public void testFilterWithQueryParameters() throws Exception {
        final String baseUri = "http://localhost:8080/rest";
        final MultivaluedMap<String, String> parameters = new MultivaluedMapImpl();
        parameters.add("param1", "value1");
        parameters.add("param2", "value2.1");
        parameters.add("param2", "value2.2");
        testFilter(baseUri, parameters);
    }

    @Test
    public void testFilterWithIPv6Address() throws Exception {
        final String baseUri = "http://[2600:1f18:674c:af01:46b3:e49e:6ac3:ff7b]:8080/rest";
        testFilter(baseUri, new MultivaluedMapImpl());
    }

    @Test
    public void testDoNotFilterWhenExtensionNotMatched() throws Exception {
        testDoNotFilter("resource.myextension");
    }

    @Test
    public void testDoNotFilterWhenNoExtension() throws Exception {
        testDoNotFilter("resource");
    }

    @Test
    public void testDoNotFilterWhenExcluded() throws Exception {
        testDoNotFilter("excluded/resource.json");
    }

    private void testFilter(String baseUri, MultivaluedMap<String, String> queryParameters) throws Exception {
        final MultivaluedMapImpl headers = new MultivaluedMapImpl();

        when(containerRequest.getAbsolutePath()).thenReturn(new URI(baseUri + "application.json"));
        when(containerRequest.getBaseUri()).thenReturn(new URI(baseUri));
        when(containerRequest.getRequestHeaders()).thenReturn(headers);
        when(containerRequest.getQueryParameters()).thenReturn(queryParameters);

        jerseyFilter.filter(containerRequest);

        final List<String> acceptHeader = headers.get(ACCEPT_HEADER);
        assertEquals(1, acceptHeader.size());
        assertEquals(MediaType.APPLICATION_JSON, acceptHeader.get(0));

        verify(containerRequest).setUris(eq(new URI(baseUri)), argThat(new UriMatcher(new URI(baseUri + "application"), queryParameters)));
    }

    private void testDoNotFilter(String resourceName) throws Exception {
        final String baseUri = "http://localhost:8080/rest/";

        when(containerRequest.getBaseUri()).thenReturn(new URI(baseUri));
        when(containerRequest.getAbsolutePath()).thenReturn(new URI(baseUri + resourceName));

        jerseyFilter.filter(containerRequest);

        verify(containerRequest, never()).getRequestHeaders();
        verify(containerRequest, never()).setUris(any(), any());
    }

    private static class UriMatcher implements ArgumentMatcher<URI> {
        private final URI requestUri;
        private final MultivaluedMap<String, String> parameters;

        UriMatcher(URI requestUri, MultivaluedMap<String, String> parameters) {
            this.requestUri = requestUri;
            this.parameters = parameters;
        }

        public boolean matches(URI actual) {
            if (!actual.toString().startsWith(requestUri.toString())) {
                return false;
            }

            // check query parameters
            final String queryParametersString = '&' + actual.getQuery() + '&';
            for (Map.Entry<String, List<String>> queryParameter : parameters.entrySet()) {
                for (String value : queryParameter.getValue()) {
                    if (!queryParametersString.contains('&' + queryParameter.getKey() + '=' + value + '&')) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
