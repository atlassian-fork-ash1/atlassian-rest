package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import com.atlassian.plugins.rest.common.security.AuthorisationException;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestAdminOnlyResourceFilter {
    private AdminOnlyResourceFilter adminOnlyResourceFilter;
    @Mock
    private UserManager mockUserManager;
    @Mock
    private ContainerRequest containerRequest;
    private UserKey userKey;

    @Before
    public void setUp() {
        initMocks(this);
        adminOnlyResourceFilter = new AdminOnlyResourceFilter(mockUserManager);
        userKey = new UserKey("dusan");
    }

    @Test
    public void filterPassed() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isAdmin(userKey)).thenReturn(true);
        assertSame(containerRequest, adminOnlyResourceFilter.getRequestFilter().filter(containerRequest));
        verify(mockUserManager).isAdmin(userKey);
    }

    @Test(expected = AuthenticationRequiredException.class)
    public void filterRejectedNoLogin() {
        adminOnlyResourceFilter.getRequestFilter().filter(containerRequest);
    }


    @Test
    public void filterRejectedNotAdmin() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        try {
            adminOnlyResourceFilter.getRequestFilter().filter(containerRequest);
            fail("AuthorisationException not thrown");
        } catch (AuthorisationException ae) {
            verify(mockUserManager).isAdmin(userKey);
        }
    }

}
