/**
 * This class is here to unit-test behaviour regarding the default package
 *
 * @see com.atlassian.plugins.rest.common.security.jersey.TestCorsResourceFilterFactory#testThatResourceInDefaultPackageIsTolerated()
 */
public class ClassInDefaultPackageForTesting {
}
