package com.atlassian.rest.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares an actual type accepted in the request body by a REST method so that the WADL generator can create JSON schemas.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequestType
{
    /**
     * Type of the request body.
     */
    Class<?> value();

    /**
     * If the request class is generic you are supposed to provide all its generic types here.
     */
    Class<?>[] genericTypes() default {};
}