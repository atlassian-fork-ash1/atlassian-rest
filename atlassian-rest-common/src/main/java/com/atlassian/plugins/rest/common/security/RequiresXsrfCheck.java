package com.atlassian.plugins.rest.common.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation used to indicate that a method needs XSRF protection checking
 *
 * @since 2.4
 * @deprecated since 3.0 this annotation will be removed in 4.0.
 * This annotation is only required on @GET annotated resources.
 * Resources annotated with a mutative method such as @POST no longer
 * required this annotation as they are XSRF protected by default.
 *
 * To exclude a resource method from XSRF protection use the
 * {@link com.atlassian.annotations.security.XsrfProtectionExcluded} annotation.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequiresXsrfCheck {
}
