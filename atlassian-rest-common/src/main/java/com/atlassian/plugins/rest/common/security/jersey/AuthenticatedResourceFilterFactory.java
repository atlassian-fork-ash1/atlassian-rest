package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.ws.rs.ext.Provider;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * A {@link ResourceFilterFactory} that checks whether the client is authenticated or not.
 *
 * @see AuthenticatedResourceFilter
 */
@Provider
public class AuthenticatedResourceFilterFactory implements ResourceFilterFactory {
    private final AuthenticationContext authenticationContext;

    public AuthenticatedResourceFilterFactory(final AuthenticationContext authenticationContext) {
        this.authenticationContext = requireNonNull(authenticationContext);
    }

    public List<ResourceFilter> create(AbstractMethod abstractMethod) {
        return Collections.singletonList(new AuthenticatedResourceFilter(abstractMethod, authenticationContext));
    }
}
