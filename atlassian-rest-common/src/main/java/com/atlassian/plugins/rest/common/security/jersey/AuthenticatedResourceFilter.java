package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

import static java.util.Objects.requireNonNull;

/**
 * This is a Jersey resource filter that checks wether the current client is authenticated or not. If the client is
 * not authenticated then an {@link AuthenticationRequiredException} is thrown.
 * <p>
 * Resources can be marked as not needing authentication by using the {@link AnonymousAllowed} annotation.
 */
class AuthenticatedResourceFilter implements ResourceFilter, ContainerRequestFilter {
    private final AbstractMethod abstractMethod;
    private final AuthenticationContext authenticationContext;

    public AuthenticatedResourceFilter(AbstractMethod abstractMethod, AuthenticationContext authenticationContext) {
        this.abstractMethod = requireNonNull(abstractMethod, "abstractMethod can't be null");
        this.authenticationContext = requireNonNull(authenticationContext, "authenticationContext can't be null");
    }

    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    public ContainerRequest filter(ContainerRequest request) {
        if (!isAnonymousAllowed() && !isClientAuthenticated()) {
            throw new AuthenticationRequiredException();
        }
        return request;
    }

    private boolean isAnonymousAllowed() {
        return (abstractMethod.getMethod() != null && abstractMethod.getMethod().getAnnotation(AnonymousAllowed.class) != null)
                || abstractMethod.getResource().getAnnotation(AnonymousAllowed.class) != null;
    }

    private boolean isClientAuthenticated() {
        return authenticationContext.isAuthenticated();
    }
}
