package com.atlassian.plugins.rest.common.expand.resolver;

import com.atlassian.plugins.rest.common.expand.EntityExpander;

import static java.util.Objects.requireNonNull;


abstract class AbstractEntityExpanderResolver implements EntityExpanderResolver {
    public final <T> boolean hasExpander(T instance) {
        return hasExpander(requireNonNull(instance).getClass());
    }

    @SuppressWarnings("unchecked")
    public final <T> EntityExpander<T> getExpander(T instance) {
        return (EntityExpander<T>) getExpander(requireNonNull(instance).getClass());
    }
}
