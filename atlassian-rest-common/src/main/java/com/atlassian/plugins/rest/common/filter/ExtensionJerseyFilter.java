package com.atlassian.plugins.rest.common.filter;

import com.atlassian.annotations.tenancy.TenantAware;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.Provider;
import java.net.URI;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.atlassian.annotations.tenancy.TenancyScope.TENANTLESS;
import static javax.ws.rs.core.MediaType.APPLICATION_ATOM_XML;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * A filter to handle URI with extensions. It will set the correct accept header for each extension and remove the
 * extension from the URI to allow for normal processing of the request.
 * <p>
 * Currently supported extension and their matching headers are defined in
 * {@link #EXTENSION_TO_ACCEPT_HEADER the extension to header mapping}.
 * <p>
 * One can exclude URIs from being processed by this filter. Simply create the filter with a collection of
 * {@link Pattern patterns} to be excluded.
 * <p>
 * <strong>Example:</strong> URI {@code http://localhost:8080/app/rest/my/resource.json} would be automatically
 * mapped to URI {@code http://localhost:8080/app/rest/my/resource} with its {@code accept} header set to
 * {@code application/json}
 */
@Provider
public class ExtensionJerseyFilter implements ContainerRequestFilter {
    private static final String DOT = ".";

    private final Collection<Pattern> pathExcludePatterns;

    public ExtensionJerseyFilter() {
        pathExcludePatterns = new LinkedList<>();
    }

    public ExtensionJerseyFilter(Collection<String> pathExcludePatterns) {
        Validate.notNull(pathExcludePatterns);
        this.pathExcludePatterns = compilePatterns(pathExcludePatterns);
    }

    @TenantAware(TENANTLESS)
    final static Map<String, String> EXTENSION_TO_ACCEPT_HEADER = new ImmutableMap.Builder<String, String>()
            .put("txt", TEXT_PLAIN)
            .put("htm", TEXT_HTML)
            .put("html", TEXT_HTML)
            .put("json", APPLICATION_JSON)
            .put("xml", APPLICATION_XML)
            .put("atom", APPLICATION_ATOM_XML)
            .build();

    public ContainerRequest filter(ContainerRequest request) {
        final String absoluteUri = request.getAbsolutePath().toString();
        final String extension = StringUtils.substringAfterLast(absoluteUri, DOT);
        if (shouldFilter("/" + StringUtils.difference(request.getBaseUri().toString(), absoluteUri), extension)) {
            request.getRequestHeaders().putSingle(HttpHeaders.ACCEPT, EXTENSION_TO_ACCEPT_HEADER.get(extension));
            final String absoluteUriWithoutExtension = StringUtils.substringBeforeLast(absoluteUri, DOT);
            request.setUris(request.getBaseUri(), getRequestUri(absoluteUriWithoutExtension, request.getQueryParameters()));
        }
        return request;
    }

    private boolean shouldFilter(String restPath, String extension) {
        for (Pattern pattern : pathExcludePatterns) {
            if (pattern.matcher(restPath).matches()) {
                return false;
            }
        }
        return EXTENSION_TO_ACCEPT_HEADER.containsKey(extension);
    }

    private URI getRequestUri(String absoluteUriWithoutExtension, Map<String, List<String>> queryParams) {
        final UriBuilder requestUriBuilder = UriBuilder.fromUri(absoluteUriWithoutExtension);
        for (Map.Entry<String, List<String>> queryParamEntry : queryParams.entrySet()) {
            for (String value : queryParamEntry.getValue()) {
                requestUriBuilder.queryParam(queryParamEntry.getKey(), value);
            }
        }
        return requestUriBuilder.build();
    }

    private Collection<Pattern> compilePatterns(Collection<String> pathExcludePatterns) {
        final Collection<Pattern> patterns = new LinkedList<>();
        for (String pattern : pathExcludePatterns) {
            patterns.add(Pattern.compile(pattern));
        }
        return patterns;
    }
}
