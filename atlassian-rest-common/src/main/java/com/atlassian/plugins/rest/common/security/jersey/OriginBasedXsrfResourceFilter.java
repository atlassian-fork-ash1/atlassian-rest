package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.http.method.Methods;
import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;
import com.sun.jersey.spi.container.ContainerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Protects browsers against XSRF attacks where the origin of a request would not
 * otherwise be permitted by the same origin policy or CORS.
 *
 * @since 2.9.21
 */
class OriginBasedXsrfResourceFilter extends XsrfResourceFilter {

    private static final Logger log = LoggerFactory.getLogger(OriginBasedXsrfResourceFilter.class);

    public ContainerRequest filter(final ContainerRequest request) {
        if (!Methods.isMutative(request.getMethod()) || !isLikelyToBeFromBrowser(request)) {
            return request;
        }
        if (passesAdditionalBrowserChecks(request)) {
            return request;
        }
        else if (request.getMediaType() != null &&
                isXsrfable(request.getMethod(), request.getMediaType())
                ) {
            logXsrfFailureButNotBeingEnforced(request, log);
            return request;
        }
        throw new XsrfCheckFailedException();
    }

}
