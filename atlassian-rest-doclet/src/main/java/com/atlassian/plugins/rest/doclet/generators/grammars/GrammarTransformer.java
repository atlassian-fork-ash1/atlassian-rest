package com.atlassian.plugins.rest.doclet.generators.grammars;

import java.util.List;

/*
 * Internal class, don't use directly
 * 
 * That class is collection of helper function to transform old grammars.xml parse tree into
 * tree of new domain objects, see package-info for detailed explanation
 * 
 * @author pandronov
 */
public class GrammarTransformer {
    public static com.sun.research.ws.wadl.Grammars transform(Grammars grm) {
        com.sun.research.ws.wadl.Grammars result = new com.sun.research.ws.wadl.Grammars();
        transformDocument(grm.getDoc(), result.getDoc());
        transformInclude(grm.getInclude(), result.getInclude());
        result.getAny().addAll(grm.getAny());
        return result;
    }

    private static void transformDocument(List<Doc> src, List<com.sun.research.ws.wadl.Doc> dst) {
        for (Doc doc : src) {
            dst.add(transform(doc));
        }
    }

    private static void transformInclude(List<Include> src, List<com.sun.research.ws.wadl.Include> dst) {
        for (Include inc : src) {
            dst.add(transform(inc));
        }
    }

    private static com.sun.research.ws.wadl.Doc transform(Doc doc) {
        com.sun.research.ws.wadl.Doc result = new com.sun.research.ws.wadl.Doc();
        result.setLang(doc.getLang());
        result.setTitle(doc.getTitle());
        result.getOtherAttributes().putAll(doc.getOtherAttributes());
        result.getContent().addAll(doc.getContent());
        return result;
    }

    private static com.sun.research.ws.wadl.Include transform(Include inc) {
        com.sun.research.ws.wadl.Include result = new com.sun.research.ws.wadl.Include();
        transformDocument(inc.getDoc(), result.getDoc());
        result.setHref(inc.getHref());
        result.getOtherAttributes().putAll(inc.getOtherAttributes());
        return result;
    }
}
