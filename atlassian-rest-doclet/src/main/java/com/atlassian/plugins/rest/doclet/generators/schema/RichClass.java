package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.annotations.tenancy.TenantAware;
import com.google.common.base.Preconditions;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.atlassian.annotations.tenancy.TenancyScope.TENANTLESS;
import static java.util.Collections.unmodifiableList;

/**
 * This is a wrapper over class/type which provides advanced support for generics.
 */
public class RichClass
{
    private final Class<?> actualClass;
    private final List<Type> genericTypes;

    @TenantAware(TENANTLESS)
    private final Map<String, Type> typeVariablesResolution;

    private RichClass(final Class<?> actualClass, final List<Type> genericTypes, final Map<String, Type> typeVariablesResolution)
    {
        this.actualClass = actualClass;
        this.genericTypes = genericTypes;
        this.typeVariablesResolution = new HashMap<>(typeVariablesResolution);

        this.typeVariablesResolution.putAll(typeVariablesResolutionsFromType(actualClass.getGenericSuperclass()));
        for (Type type : actualClass.getGenericInterfaces())
        {
            this.typeVariablesResolution.putAll(typeVariablesResolutionsFromType(type));
        }
    }

    public static RichClass of(final Class<?> actualClass, final Class<?>... genericTypes)
    {
        Preconditions.checkArgument(actualClass.getTypeParameters().length == genericTypes.length, actualClass + " has " + actualClass.getTypeParameters().length + " type parameters but " + genericTypes.length + " were provided");

        Type type = genericTypes.length == 0 ? actualClass : new ParameterizedType() {

            @Override
            public java.lang.reflect.Type[] getActualTypeArguments()
            {
                return genericTypes;
            }

            @Override
            public java.lang.reflect.Type getRawType()
            {
                return actualClass;
            }

            @Override
            public java.lang.reflect.Type getOwnerType()
            {
                return null;
            }
        };

        return RichClass.of(type);
    }

    public static RichClass of(Type type)
    {
        return create(type, Collections.<String, Type>emptyMap());
    }

    private static RichClass create(Type type, Map<String, Type> genericTypesMapping)
    {
        if (type instanceof Class)
        {
            Class<?> clazz = (Class<?>) type;
            if (clazz.isArray())
            {
                return new RichClass(List.class, Collections.<Type>singletonList(clazz.getComponentType()), Collections.<String, Type>emptyMap());
            }
            else
            {
                return new RichClass(clazz, Collections.<Type>emptyList(), Collections.<String, Type>emptyMap());
            }
        }
        else if (type instanceof ParameterizedType)
        {
            Class<?> actualClass = (Class<?>) ((ParameterizedType) type).getRawType();
            List<Type> actualTypeArguments = Arrays.asList(((ParameterizedType) type).getActualTypeArguments());
            Map<String, Type> typeVariablesResolution = typeVariablesResolution(actualClass.getTypeParameters(), actualTypeArguments);
            typeVariablesResolution.putAll(genericTypesMapping);
            return new RichClass(actualClass, actualTypeArguments, typeVariablesResolution);
        }
        else if (type instanceof WildcardType)
        {
            return create(((WildcardType) type).getUpperBounds()[0], genericTypesMapping);
        }
        else if (type instanceof TypeVariable)
        {
            Type declaredType = genericTypesMapping.get(((TypeVariable) type).getName());
            Preconditions.checkState(declaredType != null, "unresolved type variable: " + type);
            return create(declaredType, genericTypesMapping);
        }
        else
        {
            throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    public RichClass createContainedType(Type type)
    {
        return create(type, typeVariablesResolution);
    }

    private static Map<String, Type> typeVariablesResolutionsFromType(Type type)
    {
        if (type instanceof ParameterizedType)
        {
            Class<?> actualClass = (Class<?>) ((ParameterizedType) type).getRawType();
            List<Type> actualTypeArguments = Arrays.asList(((ParameterizedType) type).getActualTypeArguments());
            return typeVariablesResolution(actualClass.getTypeParameters(), actualTypeArguments);
        }
        return Collections.emptyMap();
    }

    private static <T> Map<String, Type> typeVariablesResolution(final TypeVariable<Class<T>>[] typeParameters, final List<Type> actualTypeArguments)
    {
        Map<String, Type> result = new HashMap<>();
        for (int i = 0; i < typeParameters.length; i++)
        {
            Type actualType = actualTypeArguments.get(i);
            if (!(actualType instanceof TypeVariable))
            {
                result.put(typeParameters[i].getName(), actualType);
            }
        }
        return result;
    }

    public Class<?> getActualClass()
    {
        return actualClass;
    }

    public boolean hasGenericType()
    {
        return genericTypes.size() > 0;
    }

    public List<RichClass> getGenericTypes()
    {
        return unmodifiableList(genericTypes.stream().map(this::createContainedType).collect(Collectors.toList()));
        
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RichClass that = (RichClass) o;
        
        return Objects.equals(this.actualClass, that.actualClass) &&
                Objects.equals(this.genericTypes, that.genericTypes) &&
                Objects.equals(this.typeVariablesResolution, that.typeVariablesResolution);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(actualClass, genericTypes, typeVariablesResolution);
    }

    @Override
    public String toString()
    {
        return actualClass.getSimpleName() + " " + genericTypes;
    }
}
