package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;

public class JsonOperations {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
    }

    public static String toJson(Object bean) {
        try {
            return objectMapper.writeValueAsString(bean);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
