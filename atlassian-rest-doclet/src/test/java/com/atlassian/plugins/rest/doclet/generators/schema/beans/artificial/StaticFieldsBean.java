package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class StaticFieldsBean
{
    private final static String staticField = "static field";
}
