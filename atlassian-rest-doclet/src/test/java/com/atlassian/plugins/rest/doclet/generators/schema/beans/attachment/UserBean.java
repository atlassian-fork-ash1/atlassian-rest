package com.atlassian.plugins.rest.doclet.generators.schema.beans.attachment;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme.GroupJsonBean;

import java.net.URI;
import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @since v4.2
 */
@XmlRootElement (name = "user")
public class UserBean
{
    @XmlElement
    private URI self;

    @XmlElement
    private String key;

    @XmlElement
    private String name;

    @XmlElement
    private String emailAddress;

    @XmlElement
    private Map<String, URI> avatarUrls;

    @XmlElement
    private String displayName;

    @XmlElement
    private boolean active;

    @XmlElement
    private String timeZone;

    @XmlElement
    private String locale;

    @Expandable
    @XmlElement
    private SimpleListWrapper<GroupJsonBean> groups;

    @XmlAttribute (name = "expand")
    private String expand;
}

