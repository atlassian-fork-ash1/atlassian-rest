package com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.unversioned;

import com.sun.jersey.api.model.AbstractResource;

import javax.ws.rs.POST;

/**
 * This class is used in {@link AtlassianWadlGeneratorResourceDocSupportTest}.
 * {@link UnversionedResource} is passed to the Constructor of {@link AbstractResource}, where its package information is used to find
 * an associated rest module, in atlassian-plugin.xml.
 * The plugin xml, defines the path and version for the rest API for this resource as :
 * <rest key="versionless-resource" path="/vr" version="none">
 *     <package>com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.unversioned</package>
 * </rest>
 */
public class UnversionedResource {
    @POST
    public void postMethod() {
    }
}