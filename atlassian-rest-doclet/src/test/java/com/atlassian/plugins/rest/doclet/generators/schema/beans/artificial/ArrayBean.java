package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class ArrayBean
{
    private int[] numbers;
}
