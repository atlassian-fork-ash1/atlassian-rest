package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import com.atlassian.rest.annotation.RestProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class DescriptionAndRequiredBean {
    @RestProperty(description = "legacy description", required = true)
    private String legacy;
    @JsonPropertyDescription("newJackson description")
    @JsonProperty(required = true)
    private String newJackson;
}
