package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import java.util.Objects;

import com.google.common.base.MoreObjects;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.node.ObjectNode;

@JsonAutoDetect
public class BeanWithObjectNode
{
    private final ObjectNode properties;

    @JsonCreator
    public BeanWithObjectNode(@JsonProperty ("properties") ObjectNode properties)
    {
        this.properties = properties;
    }

    public ObjectNode getProperties()
    {
        return properties;
    }

    public static BeanWithObjectNode.Builder builder()
    {
        return new BeanWithObjectNode.Builder();
    }

    public static BeanWithObjectNode.Builder builder(BeanWithObjectNode data)
    {
        return new BeanWithObjectNode.Builder(data);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        BeanWithObjectNode that = (BeanWithObjectNode) o;

        return Objects.equals(this.getProperties(), that.getProperties());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getProperties());
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("properties", getProperties())
                .toString();
    }

    public static final class Builder
    {

        private ObjectNode properties;

        private Builder() {}

        private Builder(BeanWithObjectNode initialData)
        {

            this.properties = initialData.getProperties();
        }

        public Builder setProperties(ObjectNode properties)
        {
            this.properties = properties;
            return this;
        }

        public BeanWithObjectNode build()
        {
            return new BeanWithObjectNode(properties);
        }
    }
}
