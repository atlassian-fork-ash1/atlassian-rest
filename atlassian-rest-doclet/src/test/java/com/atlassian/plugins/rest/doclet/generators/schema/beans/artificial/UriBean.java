package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.net.URI;

@JsonAutoDetect
public class UriBean
{
    private URI uri;
}
