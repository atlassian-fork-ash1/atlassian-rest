package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Bean for the top level of a GET editmeta issue request.
 *
 * @since v5.0
 */
public class EditMetaBean
{
    @XmlAttribute
    @XmlElement
    private Map<String, FieldMetaBean> fields;
}
