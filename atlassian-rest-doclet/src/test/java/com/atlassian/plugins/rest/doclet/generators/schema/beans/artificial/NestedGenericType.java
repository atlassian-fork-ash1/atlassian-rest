package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import com.atlassian.rest.annotation.RestProperty;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;
import java.util.Map;

@JsonAutoDetect
public class NestedGenericType
{
    private List<List<String>> listOfLists;
    @RestProperty(pattern = "\\d+")
    private Map<String, Map<String, String>> mapOfMaps;
    private List<Map<String, Integer>> listOfPatterns;
}
