package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class InterfaceBean
{
    public interface Interface {
        String getValue();
    }

    private Interface interfaceField;
}
