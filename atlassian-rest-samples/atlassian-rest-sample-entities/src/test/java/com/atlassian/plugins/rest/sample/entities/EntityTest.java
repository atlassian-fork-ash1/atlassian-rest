package com.atlassian.plugins.rest.sample.entities;

import com.sun.jersey.core.impl.provider.entity.XMLRootObjectProvider;
import com.sun.jersey.spi.inject.Injectable;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBContext;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Arrays;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class EntityTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private HttpHeaders headers;
    @Mock
    private Providers providers;
    @Mock
    private ContextResolver<JAXBContext> ctxtResolver;

    private EntityResource entity = new EntityResource();

    @Test
    public void testApplesForOranges() {
        Apple apple = (Apple) entity.applesForOranges(new Orange("valencia"), headers).getEntity();
        assertEquals("apple-valencia", apple.getName());
        assertEquals("N/A", apple.getReqAccept());
        assertEquals("N/A", apple.getReqContentType());

        when(headers.getRequestHeader("Content-Type")).thenReturn(singletonList("application/json"));
        when(headers.getRequestHeader("Accept")).thenReturn(singletonList("application/xml"));

        apple = (Apple) entity.applesForOranges(new Orange("delfino"), headers).getEntity();
        assertEquals("apple-delfino", apple.getName());
        assertEquals("application/json", apple.getReqContentType());
        assertEquals("application/xml", apple.getReqAccept());

        when(headers.getRequestHeader("Content-Type")).thenReturn(singletonList("application/json"));
        when(headers.getRequestHeader("Accept")).thenReturn(Arrays.asList(
                "application/xml",
                "application/json",
                "image/png"));

        apple = (Apple) entity.applesForOranges(new Orange("delfino"), headers).getEntity();
        assertEquals("apple-delfino", apple.getName());
        assertEquals("application/json", apple.getReqContentType());
        assertEquals("application/xml,application/json,image/png", apple.getReqAccept());
    }

    private static InputStream streamOf(String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    @Test

    public void unmarshallingFromStringsUsesXmlAttributeValues() throws Exception {
        JAXBContext ctxt = JAXBContext.newInstance(Pear.class);

        when(ctxtResolver.getContext(Pear.class)).thenReturn(ctxt);
        when(providers.getContextResolver(JAXBContext.class, MediaType.APPLICATION_XML_TYPE)).thenReturn(ctxtResolver);

        Injectable<SAXParserFactory> spf = () -> {
            SAXParserFactory inst = SAXParserFactory.newInstance();
            inst.setNamespaceAware(true);
            return inst;
        };

        XMLRootObjectProvider rop = new XMLRootObjectProvider.App(spf, providers);

        Pear p1 = (Pear) rop.readFrom((Class) Pear.class, null, new Annotation[0],
                MediaType.APPLICATION_XML_TYPE, null,
                streamOf("<pear name='Bartlett'/>"));

        assertEquals("Bartlett", p1.getName());

        Pear p2 = (Pear) rop.readFrom((Class) Pear.class, null, new Annotation[0],
                MediaType.APPLICATION_XML_TYPE, null,
                streamOf("<pear name='Conference'/>"));

        assertEquals("Conference", p2.getName());
    }
}
