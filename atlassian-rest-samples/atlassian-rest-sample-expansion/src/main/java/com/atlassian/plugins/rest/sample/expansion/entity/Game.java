package com.atlassian.plugins.rest.sample.expansion.entity;

import com.atlassian.plugins.rest.common.expand.Expandable;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(FIELD)
public class Game {
    @XmlAttribute
    private String name;

    @XmlElement
    @Expandable
    private Players players;


    private Game() {
    }

    public Game(String name) {
        this.name = name;
    }

    public void setPlayers(Players players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public Players getPlayers() {
        return players;
    }
}
