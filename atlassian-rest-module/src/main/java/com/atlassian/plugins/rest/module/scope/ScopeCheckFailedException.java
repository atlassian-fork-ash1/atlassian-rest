package com.atlassian.plugins.rest.module.scope;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Thrown when an Scope check fails
 *
 * @since 3.2
 */
public class ScopeCheckFailedException extends WebApplicationException {

    public ScopeCheckFailedException(Response.Status status) {
        super(Response.status(status).entity("Scope Check Failed").build());
    }
}
