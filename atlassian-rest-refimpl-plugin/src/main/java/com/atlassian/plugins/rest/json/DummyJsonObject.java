package com.atlassian.plugins.rest.json;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

public class DummyJsonObject {
    @JsonSerialize
    public static class DummyJsonParentObject {
        @JsonProperty
        private final String dataFromParent;

        public DummyJsonParentObject(String dataFromParent) {
            this.dataFromParent = dataFromParent;
        }
    }

    @JsonSerialize
    public static class DummyJsonChildObject extends DummyJsonParentObject {
        @JsonProperty
        private final String dataFromChild;

        public DummyJsonChildObject(String dataFromParent, String dataFromChild) {
            super(dataFromParent);
            this.dataFromChild = dataFromChild;
        }
    }

    @JsonSerialize
    public static class DummyJsonParentObjectGeneric<T> {
        @JsonProperty
        private final T dataFromParent;

        public DummyJsonParentObjectGeneric(T dataFromParent) {
            this.dataFromParent = dataFromParent;
        }
    }

    @JsonSerialize
    public static class DummyJsonChildObjectGeneric<T> extends DummyJsonParentObjectGeneric<T> {
        @JsonProperty
        private final T dataFromChild;

        public DummyJsonChildObjectGeneric(T dataFromParent, T dataFromChild) {
            super(dataFromParent);
            this.dataFromChild = dataFromChild;
        }
    }
}
