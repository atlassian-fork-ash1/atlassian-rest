package com.atlassian.plugins.rest.autowiring;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SomeService {
    private String name = "Default service name";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
