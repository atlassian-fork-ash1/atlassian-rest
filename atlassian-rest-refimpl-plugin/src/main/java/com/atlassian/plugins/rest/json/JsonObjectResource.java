package com.atlassian.plugins.rest.json;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("json")
@AnonymousAllowed
public class JsonObjectResource {
    @GET
    public JsonObject get() {
        return new JsonObject();
    }
}
