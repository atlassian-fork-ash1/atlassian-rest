package com.atlassian.plugins.rest.xsrf;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;

/**
 */
@Path("/xsrfCheck")
@AnonymousAllowed
public class XsrfCheck {

    @GET
    public String getXsrfMessage() {
        return "Request succeeded";
    }

    @POST
    public String postXsrfMessage() {
        return "Request succeeded";
    }

    @PUT
    public String putXsrfMessage() {
        return "Request succeeded";
    }

    @DELETE
    public String deleteXsrfMessage() {
        return "Request succeeded";
    }

    @Path("xsrfProtectionExcludedResource")
    @POST
    @XsrfProtectionExcluded
    public String postXsrfMessagXsrfProtectionExcluded() {
        return "Request succeeded";
    }

    @Path("requiresXsrfCheckAnnotatedResource")
    @GET
    @RequiresXsrfCheck
    public String getXsrfMessageRequiresXsrfCheck() {
        return "Request succeeded";
    }
}
