package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.expand.Expander;

import javax.ws.rs.core.UriInfo;

import static javax.xml.bind.annotation.XmlAccessType.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(ProjectSpecificationExpander.class)
public class ProjectSpecification {
    @XmlElement
    private Link link;

    @XmlAttribute
    private String title;

    @XmlElement
    private String text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public static ProjectSpecification getSpecification(UriInfo uriInfo) {
        final ProjectSpecification projectSpecification = new ProjectSpecification();
        projectSpecification.setLink(Link.self(uriInfo.getAbsolutePathBuilder().path("spec").build()));
        return projectSpecification;
    }
}
