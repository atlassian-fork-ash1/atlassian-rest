package com.atlassian.plugins.rest.json;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.atlassian.plugins.rest.json.DummyJsonObject.DummyJsonParentObject;
import com.atlassian.plugins.rest.json.DummyJsonObject.DummyJsonChildObject;
import com.atlassian.plugins.rest.json.DummyJsonObject.DummyJsonParentObjectGeneric;
import com.atlassian.plugins.rest.json.DummyJsonObject.DummyJsonChildObjectGeneric;

@Path("dummyjson")
@AnonymousAllowed
public class DummyJsonObjectResource {
    @GET
    @Path("subclass")
    public DummyJsonParentObject getSubClass() {
        return new DummyJsonChildObject("parent", "child");
    }

    @GET
    @Path("generic")
    public DummyJsonParentObjectGeneric getGeneric() {
        return new DummyJsonParentObjectGeneric<String>("parent");
    }

    @GET
    @Path("subclassgeneric")
    public DummyJsonParentObjectGeneric getSubClassGeneric() {
        return new DummyJsonChildObjectGeneric("parent", "child");
    }

    @GET
    @Path("subclassgenericexplicit")
    public DummyJsonParentObjectGeneric<String> getSubClassGenericExplicit() {
        return new DummyJsonChildObjectGeneric<String>("parent", "child");
    }
}
