package com.atlassian.plugins.rest.expand;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.SelfExpanding;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Representation bean used to test expando stuff.
 */
@XmlRootElement(name = "expand")
public class Expand {
    /**
     * The value to expect when expansion is performed.
     */
    public static final ImmutableList<String> SELF_EXPANDING_STRINGS = ImmutableList.of("one", "two");

    /**
     * An expandable field. Will get expanded by the framework if requested.
     */
    @XmlAttribute
    private List<String> selfExpanding = Lists.newArrayList();

    /**
     * This expander takes care of populating the selfExpanding field when requested.
     */
    @Expandable("selfExpanding")
    @XmlTransient
    private SelfExpanding selfExpandingExpander = new SelfExpanding() {
        public void expand() {
            selfExpanding = Lists.newArrayList(SELF_EXPANDING_STRINGS);
        }
    };

    public Expand() {
        // empty
    }

    public List<String> getSelfExpanding() {
        return selfExpanding;
    }
}
