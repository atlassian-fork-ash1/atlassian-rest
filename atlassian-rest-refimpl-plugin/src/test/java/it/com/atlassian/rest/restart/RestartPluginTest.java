package it.com.atlassian.rest.restart;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import org.junit.Test;

import static com.atlassian.rest.jersey.client.WebResourceFactory.LATEST;
import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION;
import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION_2;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.junit.Assert.assertEquals;

public class RestartPluginTest {
    @Test
    public void testGetAnonymousHelloWorldWhenNotAuthenticated() {
        assertGetAnonymousHelloWorldWhenNotAuthenticated(REST_VERSION, "Hello Anonymous World");
        assertGetAnonymousHelloWorldWhenNotAuthenticated(REST_VERSION_2, "Goodbye Anonymous World");
        WebResourceFactory.anonymous(REST_VERSION).path("restart").post();
        assertGetAnonymousHelloWorldWhenNotAuthenticated(REST_VERSION, "Hello Anonymous World");
        assertGetAnonymousHelloWorldWhenNotAuthenticated(REST_VERSION_2, "Goodbye Anonymous World");
        assertGetAnonymousHelloWorldWhenNotAuthenticated(LATEST, "Goodbye Anonymous World");
    }

    private void assertGetAnonymousHelloWorldWhenNotAuthenticated(final String version, final String expected) {
        assertEquals(expected, WebResourceFactory.anonymous(version).path("helloworld").path("anonymous").get(String.class));
    }


}