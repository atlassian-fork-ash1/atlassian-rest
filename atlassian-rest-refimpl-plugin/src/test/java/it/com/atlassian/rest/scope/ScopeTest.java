package it.com.atlassian.rest.scope;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.ws.rs.core.Cookie;

import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import static org.junit.Assert.assertEquals;

public class ScopeTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    final Cookie dummyScope = new Cookie("atlassian.scope.dummy", "true");
    final Cookie refappScope = new Cookie("atlassian.scope.refapp", "true");

    @Test
    public void testExplicitScope() {
        assertEquals("exp", doRequest("explicit-scope", refappScope));
    }

    @Test
    public void testExplicitScopeIsDummy() {
        expectStatusCode(PRECONDITION_FAILED.getStatusCode());
        doRequest("explicit-scope", dummyScope);
    }

    @Test
    public void testExplicitScopeIsNull() {
        expectStatusCode(PRECONDITION_FAILED.getStatusCode());
        doRequest("explicit-scope", null);
    }

    @Test
    public void testImplicitScope() {
        assertEquals("imp", doRequest("implicit-scope", refappScope));
    }

    @Test
    public void testImplicitScopeIsDummy() {
        expectStatusCode(PRECONDITION_FAILED.getStatusCode());
        doRequest("implicit-scope", dummyScope);
    }

    @Test
    public void testImplicitScopeIsNull() {
        expectStatusCode(PRECONDITION_FAILED.getStatusCode());
        doRequest("implicit-scope", null);
    }

    @Test
    public void testNoScoped() {
        assertEquals("non", doRequest("non-scoped", refappScope));
    }

    @Test
    public void testNoScopedIsDummy() {
        assertEquals("non", doRequest("non-scoped", dummyScope));
    }

    @Test
    public void testNoScopedIsNull() {
        assertEquals("non", doRequest("non-scoped", null));
    }

    private String doRequest(String path, Cookie scope) {
        final WebResource ws = WebResourceFactory.anonymous(WebResourceFactory.getUriBuilder()
                .path("rest")
                .path(path)
                .path(REST_VERSION)
                .path("get")
                .build());

        return scope == null ? ws.get(String.class) : ws.cookie(scope).get(String.class);
    }

    private void expectStatusCode(int statusCode) {
        expectedException.expect(new BaseMatcher<UniformInterfaceException>() {

            @Override
            public boolean matches(Object item) {
                return ((UniformInterfaceException) item).getResponse().getStatusInfo().getStatusCode() == statusCode;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Expected status code " + statusCode);
            }
        });
    }
}
